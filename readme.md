#lambda-sensor-service-scenarios

Lambda which configures test scenarios for machines with virtual sensors. 

Currently the following scenarios are supported: 

1. run 1 company with 50 machines.A machine has 20 physical sensors running at 10 Hz and 5 virtual sensors running at 10 Hz. Virtual sensor V1 = S1 + S2 + S3 + S4, etc.

2. run 10 companies with 5 machines each. A machine has 20 physical sensors running at 10 Hz and 5 virtual sensors running at 10 Hz. Virtual sensor V1 = S1 + S2 + S3 + S4, etc.

3. run 10 companies with 50 machines each. A machine has 20 physical sensors running at 10 Hz and 5 virtual sensors running at 10 Hz. Virtual sensor V1 = S1 + S2 + S3 + S4, etc. 

A demo scenario can be configured by providing an event with the following properties: 
``` language=javascript
{
    machineCount : number // the number of machines 
    companyCount : number // the number of companies
    duration : number     // how length of the duration
};
```
