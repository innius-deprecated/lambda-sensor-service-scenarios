import * as http from "http";
import { Event } from "./Event";
import { handler } from "./index";
import { Mountebank } from "./Mountebank/Mountebank";
import { ConsulService } from "./Mountebank/ConsulImposter";

const services = [
    { name: "demomachine-service", port: process.env.DEMOMACHINESERVICE_PORT },
    { name: "sensor-service", port: process.env.SENSORSERVICE_PORT },
    { name: "authentication-service" },
]

const mb = new Mountebank(services);

let consulUrl: string
mb.start().subscribe(
    (x) => consulUrl = `http://localhost:${x.port}`,
    (err) => console.log(err),
() => console.log(JSON.stringify(services)));

// create a simple web server which: 
const server = http.createServer((req, res) => {
    if (req.method === "POST") {
        let body: string = "";
        req.on("data", (chunk) => {
            body += chunk;
        });
        let event: Event;
        req.on("end", () => {
            try {
                event = JSON.parse(body)
                event.consulUrl = consulUrl;
                handler(event, {}, (err, data) => {
                    console.log(data);
                });
            } catch (error) {
                res.writeHead(400, "invalid json");
                res.end()
                return;
            }
            res.writeHead(200);
            res.end();
        });
    } else {
        res.writeHead(405);
        res.end()
    }
});

const port = process.env.PORT || 8080;
server.listen(port, () => {
    console.log(`lambda sensor service scenarios api listening at ${port}`);
});
