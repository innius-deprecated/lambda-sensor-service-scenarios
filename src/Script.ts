import { SensorKind } from "./Sensor";
import { Machine } from "./Machine";

export class Script {
    static defaultScript(machine: Machine): Script {

        let mapping: Map<String, String> = new Map()
        let n: [number, number] = [0, 0];

        machine.sensors.map(x => {
            switch (x.kind) {
                case "physical":
                    n[0]++
                    mapping.set(x.name, `S${n[0]}`);
                    break;
                case "virtual":
                    n[1]++
                    mapping.set(x.name, `V${n[1]}`);
                    break;
            }
        });

        const p = machine.sensors
            .filter(x => x.kind === "physical")
            .map(x => `${mapping.get(x.name)} = Sensor(M1, "${x.name}")`)
            .join("\n")

        const virtuals = machine.sensors
            .filter(x => x.kind === "virtual")

        const v = virtuals
            .map(x => `${mapping.get(x.name)} = VirtualSensor(M1, "${x.name}")`)
            .join("\n")

        const each = virtuals
            .map((x, i) => {
                const n = i * 4;
                return `V${i + 1}[t] = S${n + 1}[t] + S${n + 2}[t] + S${n + 3}[t] + S${n + 4}[t]`
            })
            .join("\n")

        const source = `
            object {
            M1 = Machine("plantage.com", "laser-cutter-1")
            ${p}    
            ${v}
            }
                            
            each (t:second) {
            ${each}        
            }

            output {
            ${virtuals.map((x, i) => `V${i + 1}`).join("\n")}
            }
        `

        return new Script("foo", "default-script", "default-script", source);
    }

    constructor(
        public id: string, public name: string,
        public description: string, public source: string) { }
}
