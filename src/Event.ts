export interface Event {
    machineCount: number;
    companyCount: number;
    consulUrl?: string
    duration: number    // duration in seconds
};
