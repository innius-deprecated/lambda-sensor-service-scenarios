import { Observable, Observer } from "@reactivex/rxjs";

import { HttpClient } from "../HttpClient";
import { Machine } from "../Machine";
import { Script } from "../Script";

import * as log from "../Logger/Logger";

export class SensorServiceClient {
    constructor(private client: HttpClient) { }

    /**
     * post the sensors to the sensor-service
     */
    postSensors(machine: Machine): Observable<Machine> {
        log.debug(`post metadata for machine ${machine.trn}`);
        return this.client.post(`${machine.trn}/eventing/definition`, machine.sensors)
            .map(x => machine);
    }

    /**
     * post the scripts to the sensor-service
     */
    postScript(machine: Machine, script: Script): Observable<Script> {

        return this.client.put(`${machine.trn}/eventing/scripts/${script.id}`, script).map(x => script);
    }

    /** 
     * install a script at the sensor-service
    */
    installScript(machine: Machine, script: Script): Observable<Script> {
        return this.client.post(`${machine.trn}/eventing/scripts/${script.id}/install`, {});
    }
}