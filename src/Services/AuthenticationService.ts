import { Observable } from "@reactivex/rxjs";
import { HttpClient } from "../HttpClient";
import { Machine } from "../Machine";

export type KeyType = "apikey" | "servicekey" | "machinekey";

export interface CreateServiceTokenRequest {
    audience: string,
    company: string,
    scopes: string[],
    keytype: KeyType
}

export function newRequest(machine: Machine, svc: string, scopes: string[]): CreateServiceTokenRequest {
    return {
        audience: svc,
        company: machine.trn.company,
        scopes: scopes,
        keytype: "servicekey"
    }
}

export class AuthenticationClient {
    constructor(private client: HttpClient) { }

    createServiceToken(request: CreateServiceTokenRequest): Observable<string> {
        return this.client.post("svctoken", request)            
            .map(x => x)
    }
}