import { Observable, Observer } from "@reactivex/rxjs";
import { HttpClient } from "../HttpClient";
import { Machine } from "../Machine";
import { SensorValueTemplate } from "../SensorValueTemplate";

export class DemoMachineServiceClient {

    constructor(private client: HttpClient) { }

    postTemplate(machine: Machine, template: SensorValueTemplate[]): Observable<Machine> {
        const req = {
            duration: machine.duration,
            sensors: template.map(x => {
                return {
                    sensor_id: x.sensor.name,
                    recurring: true, 
                    template: x.template
                }
            })
        }

        return this.client.post(`internal/${machine.trn}/simulator`, req).map(x => machine);
    }
}

