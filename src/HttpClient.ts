import * as https from "https";
import * as http from "http";
import * as url from "url";
import * as log from "./Logger/Logger";
import { Observable, Observer } from "@reactivex/rxjs";

export function newHttpClient(uri: string, token?: string): HttpClient {
    const _uri = url.parse(uri);

    let options: http.RequestOptions = {
        protocol: _uri.protocol,
        port: parseInt(_uri.port, 10),
        path: _uri.path,
        headers: {
            "Content-Type": "application/json"
        },
        hostname: _uri.hostname,
    }

    if (token) {
        options.headers["Authorization"] = `bearer ${token}`
    }

    return new HttpClientImplementation(options);
}

export interface HttpClient {
    withServiceName(name: string): HttpClient
    post(path: string, payload: any): Observable<any>
    put(path: string, payload: any): Observable<any>
    get(path: string): Observable<any>
}

class HttpClientImplementation {
    constructor(public options: http.RequestOptions) { }

    private serviceName: string;

    private clone(): HttpClientImplementation {
        return new HttpClientImplementation(this.options);
    }

    withServiceName(name: string): HttpClient {
        let client = this.clone();
        client.serviceName = name;
        return client;
    }

    post(path: string, payload?: any): Observable<any> {
        const options = Object.assign({}, this.options, { method: "POST", path: this.path(path) });
        return this.doRequest(options, payload)
    }

    put(path: string, payload: any): Observable<any> {
        const options = Object.assign({}, this.options, { method: "PUT", path: this.path(path) });
        return this.doRequest(options, payload)
    }

    get(path: string, options?: http.RequestOptions): Observable<any> {
        const opt = Object.assign({}, options || this.options, { method: "GET", path: this.path(path) });
        return this.doRequest(opt, {})
    }

    private path(path: string): any {
        let p: String = path;

        if (this.options.hostname !== "localhost") {
            p = this.serviceName ? `${this.serviceName}/${path}` : path
        }

        return this.options.path + p;
    }

    private doRequest(options: http.RequestOptions, payload?: any): Observable<any> {

        let obs: Observable<any> = Observable.create((o: Observer<any>) => {
            let data = JSON.stringify(payload);

            options.headers["Content-Length"] = Buffer.byteLength(data);

            log.debug("http request options:", options);
            log.debug("http request payload:", payload);

            httpClient(options).request(options, (res: http.IncomingMessage) => {
                let data = "";

                res.on("data", (chunk: string) => {
                    if (o.isUnsubscribed) { return };
                    data += chunk;
                });
                res.on("end", () => {
                    if (o.isUnsubscribed) { return };
                    if (data !== "") { log.debug("http response: ", data) }
                    o.next(data);
                    o.complete();
                });

                res.on("error", (err: Error) => {
                    if (o.isUnsubscribed) { return };
                    o.error(err)
                });

                if ([200, 201].indexOf(res.statusCode) < 0) {
                    console.log(data)
                    o.error(new Error(
                        `${options.path} returned an unexpected status: status: ${res.statusCode}; message: ${res.statusMessage}`));
                }
            })
                .end(data);
        });
        return obs;
    }
}

function httpClient(options: http.RequestOptions): any {
    if (options.protocol === "https:") {
        return https;
    }
    return http;
}
