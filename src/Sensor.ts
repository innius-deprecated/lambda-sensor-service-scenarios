import { Threshold } from "./Threshold";

export type SensorKind = "physical" | "virtual";

export type SensorType = "temperature";

export type SensorValueType = "integer" | "double" | "string";

export type MissingValueStrategy = "previous";

export class SensorValue {
    type: SensorValueType
    unitOfMeasure: String
    decimalplaces: number
    missing: MissingValueStrategy
}

export class Sensor {
    name: String
    description: String
    kind: SensorKind
    type: SensorType
    value: SensorValue
    physicalid: String
    thresholds: Threshold[]
    capacity: number
    tags: {}
    visible: boolean
    propagate: boolean
    frequency: String
}

export function virtualSensor(name: string): Sensor {
    return {
        name: name,
        description: "a virtual sensor",
        kind: "virtual",
        type: "temperature",
        value: {
            type: "double",
            unitOfMeasure: "",
            decimalplaces: 3,
            missing: "previous"
        },
        physicalid: "x-123",
        thresholds: [],
        capacity: 10,
        tags: {},
        frequency: "1Hz",
        visible: true,
        propagate: true,
    }
}

export function physicalSensor(name: string): Sensor {
    return {
        name: name,
        description: "a physical sensor",
        kind: "physical",
        type: "temperature",
        value: {
            type: "double",
            unitOfMeasure: "",
            decimalplaces: 3,
            missing: "previous"
        },
        physicalid: "x-123",
        thresholds: [],
        capacity: 10,
        tags: {},
        frequency: "1Hz",
        visible: true,
        propagate: true,
    }
}
