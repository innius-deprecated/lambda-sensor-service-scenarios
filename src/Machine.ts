import { Observable, Observer } from "@reactivex/rxjs";
import * as uuid from "node-uuid";
import { TRN, ResourceType } from "@toincrease/node-trn";
import { range } from "./Utils";

import { Sensor, SensorType, physicalSensor, virtualSensor } from "./Sensor";
import { Script } from "./Script";
import { SensorValueTemplate, defaultTemplate } from "./SensorValueTemplate";

export class Machine {
    sensors: Sensor[];
    scripts: Script[];
    values: SensorValueTemplate[];
    duration: number // defines how long a machine is running (in seconds)

    constructor(public trn: TRN) {
        this.scripts = [];
        this.sensors = [];
        this.values = [];
        this.duration = 60 * 60 * 10
    }

    /**
    * return a machine with default sensors
    */
    withDefaultSensors(): Machine {
        this.sensors = this.sensors.concat(
            range(20).map(x => physicalSensor(`physical-sensor-${x + 1}`)),
            range(5).map(x => virtualSensor(`virtual-sensor-${x + 1}`)));
        return this
    }

    /**
    * return a machine with default scripts 
    */
    withDefaultScripts(): Machine {
        this.scripts.push(Script.defaultScript(this));

        return this;
    }

    /** 
    * return a machine with default sensor values 
    */
    withDefaultSensorValues(duration: number): Machine {
        this.duration = duration;
        this.values = this.sensors
            .filter(x => x.kind === "physical")
            .map(defaultTemplate)

        return this
    }
}
