export function range(n: number): number[] {
    let arr: number[] = [];
    for (var i = 0; i < n; i++) {
        arr.push(i)
    }
    return arr;
}
