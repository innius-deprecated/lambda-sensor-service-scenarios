import { Observable, Observer } from "@reactivex/rxjs";
import * as logger from "../Logger/Logger";
import { HttpClient } from "../HttpClient";

export function endpoint(name: string): string {
    //return "http://52.90.49.55:8500";
    return "http://" + name + ".consul.to-increase.io:8500";
}

interface Node {
    Service: Service
}

interface Service {
    Address: string
    Port: number
}

export class ConsulClient {
    constructor(private client: HttpClient) { }

    resolveService(name: string): Observable<string> {
        return this.client.get(`v1/health/service/${name}?passing&stale`)
            .map(x => <Node[]>JSON.parse(x))
            .map(x => x[Math.floor(Math.random() * x.length)]) // return a random node.
            .map(x => `http://${x.Service.Address}:${x.Service.Port}`);
    }
} 