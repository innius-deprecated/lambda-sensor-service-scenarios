import { assert } from "chai";
import { handler } from "./index";
import { Mountebank } from "./Mountebank/Mountebank";
import { ConsulService } from "./Mountebank/ConsulImposter";
import { Machine } from "./Machine";

import * as log from "./Logger/Logger";
import { range } from "./Utils";

describe("Handler", function (): void {
    this.timeout(15000);
    let id = "3309a51b-f6cf-4626-9edd-4ec025a7fa7f";
    let consulUrl: string

    before((done) => {
        const mb = new Mountebank([
            { name: "demomachine-service" },
            { name: "sensor-service" },
            { name: "authentication-service" },
        ]);

        mb.start().subscribe(
            (x) => consulUrl = `http://localhost:${x.port}`,
            (err) => done(err),
            done
        )
    })

    describe("a scenario with 1 company with 10 machines", () => {
        it("it should create 10 machines", (done) => {
            let event = {
                consulUrl: consulUrl,
                machineCount: 10,
                companyCount: 1,
                duration: 10,
            };

            handler(event, {}, (err, data) => {                
                assert.isNull(err);
                assert.equal(data.length, 10);
                done()
            })
        });
    });

    describe("a scenario with 10 companies with 5 machines", () => {
        it("should create 50 machines", (done) => {
            let event = {
                consulUrl: consulUrl,
                machineCount: 5,
                companyCount: 10,
                duration: 10,
            };

            handler(event, {}, (err, data) => {
                assert.isNull(err);                
                assert.equal(data.length, event.companyCount * event.machineCount);                
                done()
            })
        });
    });
});
