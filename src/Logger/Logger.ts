export enum LogLevel {
    debug,
    info,
    error
}

let current_log_level: LogLevel = LogLevel.info;

export function set_log_level(level: LogLevel): void {
    current_log_level = level;
};

export function info(message: string, data?: any): void {
    log(LogLevel.info, message, data);
};

export function error(message: string, data?: any): void {
    log(LogLevel.error, message, data);
};

export function debug(message: string, data?: any): void {
    log(LogLevel.debug, message, data);
};

function log(level: LogLevel, message: string, data: any): void {
    if (level >= current_log_level) {
        console.log(message);
        if (data) { console.log(data) }
    }
}