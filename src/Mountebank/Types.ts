export interface Stub {
    predicates?: Predicate[]
    responses?: Response[]
}

export interface Predicate { }

export interface EqualPredicate extends Predicate {
    equals: {
        path: string
    }
}

export interface Response { }

export class IsResponse implements Response {
    is: {
        statusCode: number,
        body?: any,
    }
}

export interface Imposter {
    protocol: string;
    stubs?: Stub[];
    port?: number;
}
