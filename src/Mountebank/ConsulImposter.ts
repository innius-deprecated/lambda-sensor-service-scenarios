import { Imposter, Stub, IsResponse } from "./Types";

export interface ConsulService {
    name: string
    address?: string
    port?: number
}

export class ConsulImposter implements Imposter {
    protocol: string = "http";
    stubs: Stub[] = []

    constructor(public services: ConsulService[], private defaultPort: number) {
        this.stubs = services.map(serviceStub(this.defaultPort))
    }
}

function serviceStub(defaultPort: number): (service: ConsulService) => Stub {
    return function (service: ConsulService): Stub {
        service.port = service.port || defaultPort;
        service.address = service.address || "127.0.0.1";
        
        return {
            "predicates": [
                {
                    "equals": {
                        "path": `/v1/health/service/${service.name}`

                    }
                }
            ],
            "responses": [
                serviceResponse(service)
            ]
        }
    }
}

function serviceResponse(service: ConsulService): IsResponse {
    return {
        "is": {
            "statusCode": 200,
            "body": [
                {
                    "Service": {
                        "Service": service.name,
                        "Address": service.address,
                        "Port": service.port,
                    }
                }
            ]
        }
    }
}