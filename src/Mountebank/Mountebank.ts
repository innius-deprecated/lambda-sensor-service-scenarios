import { Observable, Observer } from "@reactivex/rxjs";
import { HttpClient, newHttpClient } from "../HttpClient";

import { Imposter, Stub, IsResponse } from "./Types";
import {ConsulImposter, ConsulService} from "./ConsulImposter"
import {ServiceImposter} from "./ServiceImposter";

export class Mountebank {
    constructor(private services: ConsulService[]) {}

    start(): Observable<Imposter> {
        let client = newHttpClient("http://localhost:2525");

        return client.post("imposters", new ServiceImposter())
            .map(x => <Imposter>JSON.parse(x))
            .flatMap(imp => {                
                return client.post("imposters", new ConsulImposter(this.services, imp.port))
                    .map(x => <Imposter>JSON.parse(x))
            })
    }
}