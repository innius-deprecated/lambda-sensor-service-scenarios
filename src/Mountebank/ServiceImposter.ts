import { Imposter, Stub, IsResponse , EqualPredicate} from "./Types";

export class ServiceImposter implements Imposter {
    protocol : string = "http";
    stubs : Stub[] = [];
    constructor() {
        this.stubs = [
            {
                predicates: [
                    <EqualPredicate>{
                        equals: {
                            path: "/svctoken"
                        }
                    },
                ],
                responses: [
                    <IsResponse>{
                        is: {
                            statusCode: 200,
                            body: "super-secret-awesome-service-token"
                        }
                    }
                ]
            },
            {
                "responses": [
                    {
                        "is": {
                            "statusCode": 200
                        }
                    }
                ]
            }
        ]
    }
}

