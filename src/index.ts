import { Observable } from "@reactivex/rxjs";
import * as log from "./Logger/Logger";
import * as http from "http";
import * as uuid from "node-uuid";
import { Event } from "./Event";
import { Machine } from "./Machine";
import { Script } from "./Script";
import { newHttpClient } from "./HttpClient";
import { SensorServiceClient } from "./Services/SensorService";
import { DemoMachineServiceClient } from "./Services/DemoMachineService";
import { AuthenticationClient, newRequest } from "./Services/AuthenticationService";
import { TRN, ResourceType } from "@toincrease/node-trn";

import * as url from "url"
import { ConsulClient, endpoint } from "./Consul/Consul";


export function handler(event: Event, context: any, callback: (err?: any, data?: string[]) => void): void {
    log.set_log_level(process.env.LOGLEVEL || log.LogLevel.info);
    log.debug("received event: ", event)

    const consul = new ConsulClient(newHttpClient(event.consulUrl || endpoint(process.env.STACK_NAME)))
    const companies = Observable.range(0, event.companyCount);

    const result = companies
        .flatMap(c => {
            let compid = uuid.v4()

            log.debug(`process company ${compid}`)
            let machines: Observable<Machine> = Observable.range(0, event.machineCount)
                .map(x => new Machine(new TRN(compid, ResourceType.machine, uuid.v4())))
                .map(x => x.withDefaultSensors())
                .map(x => x.withDefaultSensorValues(event.duration))
                .map(x => x.withDefaultScripts());

            return handleEvent(consul, machines);
        },1)

    let machines: string[] = [];

    result.subscribe(
        (x) => {
            log.debug(`processed machine ${x[0].trn}`)
            machines.push(x[0].trn.toString());
        },
        (err) => {
            log.error(err)
            callback(err);
        },
        () => {
            log.info(`successfully created ${event.machineCount * event.companyCount} machines`);
            callback(null, machines);
        });
}

function handleEvent(consul: ConsulClient, machines: Observable<Machine>): Observable<[Machine, Machine]> {    
    return machines.flatMap(machine => {
        log.info(`creating machine ${machine.trn}`);
        const metadata = consul.resolveService("sensor-service")
            .flatMap(svc => {
                let foo = register(svc, machine)
                return foo; 
            });

        const templates = consul.resolveService("demomachine-service")
            .flatMap(svc => {
                return registerSensorValueTemplate(svc, machine)
            });

        return Observable.combineLatest(metadata, templates);
    }, 1)

    /**
    * register a new machine at the sensor service 
    */
    function register(uri: string, machine: Machine): Observable<Machine> {
        const token = createSensorServiceToken(machine);

        return token.flatMap(token => {
            const client = new SensorServiceClient(newHttpClient(uri, token));
            return client.postSensors(machine)
                .flatMap(x => Observable.from(x.scripts)
                    .flatMap(script => client.postScript(x, script))
                    .flatMap(script => client.installScript(x, script)))
                .map(x => machine);
        });
    }

    function registerSensorValueTemplate(uri: string, machine: Machine): Observable<Machine> {
        const dclient = new DemoMachineServiceClient(newHttpClient(uri));

        return dclient.postTemplate(machine, machine.values)
            .map(x => {
                return machine;
            });
    }

    function createSensorServiceToken(machine: Machine): Observable<string> {
        return consul.resolveService("authentication-service")
            .flatMap(uri => {
                const client = new AuthenticationClient(newHttpClient(uri));
                return client.createServiceToken(newRequest(machine, "sensor-service", ["create", "edit", "delete", "view"]))
            });
    }
}
