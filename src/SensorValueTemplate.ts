import { Sensor } from "./Sensor";

export class SensorValueTemplate {
    constructor(public sensor: Sensor) {
        this.template = [];
    }
    template: SensorValue[]
}

export class SensorValue {
    tick: number;
    value: number;
}

export function defaultTemplate(sensor: Sensor, i: number): SensorValueTemplate {
    let template = new SensorValueTemplate(sensor);

    template.template = [
        { tick: 0, value: i },
    ];

    return template;
}
